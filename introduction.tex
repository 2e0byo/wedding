
\subsection{This Booklet}

Welcome to the Church of St. Cuthbert for our wedding.  We are
extremely grateful---above all in these strange, strange times---that
you are here as our guests today, to be with us, to celebrate with us
and to support us as we get married.

This booklet contains the complete text of the Wedding and Mass (the
Mass follows straight on the Wedding), in Latin and English (although
the wedding itself is in English); an international language for an
international wedding.  Thus if you wish to follow closely what is
said and done you can.  But if you do not wish to, or are not familiar
with the Mass, or at least its ancient form, you are very welcome to
sit back and watch.  The readings will be repeated in English (which
is the vernacular of \emph{most} people present today) and the homily
will of course be in English; for the rest the liturgy speaks as
eloquently in gesture as it does in word.

In this booklet the gestures the congregation adopts are indicated in
bold.  These are not instructions.  You are here as our guests, to
take part in as much or as little as you wish or feel able to.

Due to the current situation, communion will only be given to the
Bride and Groom.  There will also, sadly, by government edict, be no
congregational singing or responses.

We would ask that, in general, photographs be taken \emph{after} the
service (although we have asked a few to take some during the service,
above all when so many are absent), and that \emph{no photographs at
  all be uploaded to social media} at least for a reasonable period.
These are difficult times and we do not want to create the wrong
impression, particularly when so many are unable to gather in various
ways. Instead, please upload photographs to
\url{bit.ly/johnandemma2020} and send this link in order to share them
with people.  Could we also ask that \textbf{phones be switched to
  silent} during the service.

Since there are so many here today for whom all this will be new and
strange, there follows---if you are interested---an account of the
history of Christian Worship and the form in which it will be
celebrated today through the (tangential) lens of its language.  Not
an ordinary thing to find in a wedding Order of Service, but then I
\emph{am} a theologian, and I couldn't help myself; and it's something
to read before the service begins.  But do feel free to skip it, or
indeed to put this booklet aside.  Only, when you go, do take it with
you.

\hfill John Morris

\clearpage
\subsection{A brief history of Liturgical Language}
{\sloppybottom
Even in ordinary times the ceremony as conducted today would perhaps
have seemed strange.  Other than the wedding itself, which has always
been conducted in the vernacular, the liturgy is in the ancient
language of the Western Church---Latin.  The priest blesses%
\footnote{Although he does not \emph{marry} them, whatever colloquial
  speech may suggest: Catholic theology is and always has been
  completely clear that the couple \emph{marry each other}: the priest
  and the whole congregation are called upon to witness and to support
  a marriage which is the couple's own, and even when he pronounces
  \emph{ego vos conjungo}: I unite you, it is clear that he only
  ratifies what has already taken place.  As is sung in the Introit,
  \emph{Deus Israel conjungat vos, et ipse sit vobisum}; the God of
  Israel unite you, and be with you----and in the name of the Church
  and all present the priest pronounces that He has indeed done so.} %
the newly-married couple in Latin, prays in Latin; says and sings the
Mass in Latin.  Those parts which change most and are most directly
aimed at the congregation---the readings---are repeated in English,
which is at least the vernacular of \emph{most} present today, and the
homily will of course be in English.\footnote{Only before university
  congregations, and at great metropolitan meeting-places, in the ages
  when all educated men and women spoke at least some Latin (and
  conversational Latin was still to be found in Catholic countries
  until as late as the second half of the last century) was there ever
  \emph{preaching} in Latin.}

It was never the intention that this Latin should be a barrier or a
veil hiding the liturgy.%   Such, occasionally, has been the cry of a
% reactionary Catholicism to the challenge of the Reformation.  But
The very earliest worship of the Church was in international
languages.  Aramaic, probably, at first (Aramaic worship survives even
until this day, although much ravaged in recent years by the horrors
of successive wars in the middle east, and then the genocide of
Daesh); but Aramaic is an imported, or rather a melded language, not
biblical or second-temple Hebrew, nor any other surrounding tongue,
but the language of the market place, of the inn.  Greek---the gospels
come to us in Greek; in all probability they were composed in Greek,
but a Greek more (St. Matthew, St. Mark) or less (St. Luke) semitic,
and at times (St. John, and the Apocalypse) frankly ungrammatical.
The Liturgy, East and West, has its origins in these combinations.
That which St. Paul `received from the Lord' of the Eucharist---that
is, from the Church, which had been taught the Eucharist by Christ and
was worshipping before even the gospels had been written, the canon
(West), the Anaphora (East)---was passed on, elaborated, prayed,
watched, attended, celebrated; around it grew up theologies and
commentaries; it---not Christian faith, nor even Christian prayer, but
\emph{this specific act of Christian worship} was targeted again and
again by a Roman state grown suspicious of this bizarre new sect; for
it many were martyred.  \emph{Sine Dominico non possumus!} declared
the martyrs of Abitinæ who had gathered despite Diocletian edict to
celebrate the Eucharist on Sunday.  But what \emph{was} this thing
that they celebrated?

Originally it had been Aramaic; then, or even at the same time, it was
Greek, simple, clear, ringing and rather unimpressive Greek to a world
schooled in real rhetoric.  In the west it became Latin, the vulgar
tongue, at a period when Latin was \emph{just} starting to give way to
the various vernaculars which would eventually, many centuries later,
replace it.  The conversion of Rome gave us the terse elegance of the
collects, quite different from the hellenised semiticisms of the
psalms or the at times startling crudities of the Vulgate scriptures.
Greek survived or came back in---today the \emph{Kyrie}, elsewhere a
few more words of Greek, the \emph{Agios O Theos}, are
found---alongside antiphons which occasionally are not full sentences.
Early congregations were rarely monolingual.  For many of them---for
Christianity began on the fringes of the empire---Latin and Greek were
second, or even third or fourth languages.  Greek read in Church would
never at times have been spoken on the street (the apocalypse
repeatedly contains a phrase rendered in English as `from him who is,
and was, and will be'.  A stricter translation would be `from he---he
who is, he was!, and he who is coming': no Greek speaker could have
failed to remark on the strangeness of the grammar).  Yet it was not
corrected to follow conventional usage.  It was left intact and
explained.

As the Roman empire retreated, Christianity stayed.  Its liturgies
remained in the language which until recently had been the common
language of Europe.  Latin survived the dark ages in the monasteries
(undergoing some seismic variations in syntax and orthography) and at
times the courts.  In the high middle ages it was the natural recourse
of the new Schools, and the courts: a Latin now remarkably syntactical,
not at all unlike romance languages today.  Meanwhile the Mass
continued, with almost no textual amendments, as the vernaculars of
the day swung and stabilised into something we would recognise today.
To non-speakers it was somewhat like reading or hearing Italian as,
say, a French speaker: the sense carried, even where conversation was
impossible.  Late Mediæval England, on the eve of the Reformation,
was, as Duffy\footnote{Notably \emph{The Stripping of the Altars},
  which marshals enormous quantities of evidence from macaronic
  (alternating Latin-English) poetry and popular songs to show just
  how deeply the language of the Mass \emph{and its meaning}
  penetrated a population which certainly did not routinely converse
  in Latin.} and others have shown, a land where the language of the
Mass \emph{in its invariant parts} was widely, perhaps even
completely, understood and beloved, even as it was not spoken.  Such a
situation obtains today in Russia with Old Church Slavonic, or Greece
with the Greek liturgy, or anywhere where familiarity with multiple
languages is still common even if fluency is rare.

Then at the eve of the Reformation a new development occurred in the
West: the development of printing.  Coupled with a rise in literacy,
which had been much higher than is commonly credited in the high
middle ages, particularly among women, but had declined once again,
printing gave, to those who could afford it, the ability to spread
ideas as never before.  Pamphlets of all kinds swept Europe: to those
who have studied them they are striking above all for their badness,
their oddities of style and argument, their sudden violent asides.
Writing as a means of mass persuasion was finding its feet.  With it
came the possibility of a new access to liturgy and scripture, and the
question of translation arose.  Some advocated a complete rendering of
the services of the Church into vernaculars they claimed now stable
and developed enough to express them.  Some soon went further and
desired that they be more or less completely re-written; this, of
course, happened in Protestantism and culminated in the unwritten,
more spontaneous services of the nonconformists (although this factor
can be overplayed: early Baptists published and presumably used
liturgical books of their own crafting).  Among Catholics an early
enthusiasm for translation soon gave way to intransigence: translation
was, perhaps of necessity, extremely interpretative, whether done by
Catholics or Protestants, by Classicists or Humanists.\footnote{The
  \emph{Authorised Version} is a striking example of a deliberate
  attempt to avoid this trend and was perhaps the first conciliatory
  translation, designed to please if not Catholics then at least all
  Protestants. Catholics, meanwhile, had to contend with the
  Douay-Rheims version, with which the forward of the AV contends
  itself and on which it sometimes drew, but which is markedly
  latinate in places and usages which, unlike those many unEnglish
  usages coined by the AV, did not establish themselves.  It is,
  however, a much better way of understanding the \emph{latin text},
  and functioned well in this role with occasional amendments until
  the translation of the new Roman Rite at the end of the twentieth
  century.}  Instead, devotional books \emph{based on} what was
happening at different stages of the Mass---the entrance, the prayers
of confession, the Gloria, and so on---proliferated.

These had their origins in the middle ages.  They were indeed
extremely common: hand-copied, treasured, and passed down; but only of
course in families which could afford books, and then frequently only
among women, who could read.  Modernity and printing changed all this.
Just at the point when latin familiarity had become less common
\emph{in general}, so education and printing made literacy more
common, until soon nearly every churchgoer would take with them some
kind of book.  It would be read, quietly, to those children of the
family who could not see it.  Lowering costs in printing meant that
soon more and more had their own.  Meanwhile the Catholic response to
the Reformation---the so-called Counter-Reformation---was in full
swing.  As the harangue delivered to the Fathers of Trent at its
opening claimed, `this was all our fault'---the protestants were
\emph{right} to complain about abuses, \emph{right} to demand better
understanding of the liturgy and the scriptures, \emph{right} to want
preaching (the right to preach, in those days, being conferred only on
those who had been trained, lest others should confuse); albeit, from
a Catholic perspective, wrong in the means they took to right these
ills.  And thus just as they became more and more widespread, these
mediæval devotional books, whose exemplars are frequently
\emph{entirely} reflective on a liturgy they never quote, began to
incorporate first summaries, then translations, and then ultimately
(after fears that it would lead to illicit use for `magical rituals'
in a world awash with superstition) the text and translation of the
liturgy.  Thus Christian worship, which had always been an interaction
between the common prayer of the congregation and the specific,
hieratic prayer of the Priest or (originally) Bishop and presbytery
(the martyrs of Abitinæ, if I do not muddle my martyrdoms, declared
that they had been robbed of the Lord's Day not only \emph{by the
  edict} but \emph{by the loss of their bishop} without whom it could
not be celebrated), a structure apparent in the turnings to and from
the people by the priest, gained a new dimension: the little, richly
printed and bound book, sitting on the new kneeler in front of the new
pews (anciently, as in the East today, all stood or prostrated
themselves).

At times interposed between congregant and celebrant, the praxis
warned not to let the book become the Mass.  At the summit of the
liturgy---the consecration and elevation of the host---it was laid
aside, to gaze in adoration, as had happened since time immemorial (of
this moment the murals in the catacombs of Rome, dating to the
persecutions and some of the earliest Christian art we have, are
eloquent).  For some it was merely a distraction and was laid aside,
except perhaps for the readings, if they were not read again in the
vernacular immediately before the sermon (as they will be today). For
others---I have examined fifteenth and sixteenth century examplars at
Ushaw, lovingly inscribed with the family trees and names of the
generations which had handed them on, used and treasured them, until
at last the binding became, after centuries, too weak to support use
and a replacement was sought.  The booklet for this mass stands in
that tradition, as an aid and a guide, to be used or not as you may
wish.  Directions for congregational posture are advisory: some have
always knelt or sat all through Mass; and of course, gestures of
worship belong to those worshipping.  For some of you this will be
Mass as usual.  For others, Mass, but not quite as you are used to
it.  For others, Christian worship does not generally take this form.
Still others do not worship as Christians or at all.  We are grateful
to all of you: that you are here today; that you have come as our
guests, to be with us as we celebrate this sacrament before God and
His Church.  As our guests: to be, as a guest, valued; to do what you
are comfortable doing; to observe where you do not do.

\enlargethispage{\baselineskip}

And lastly, why does the narrative stop here, with the liturgy in
Latin and the people more or less knowing it, more or less reading
along in translation?  In a sense it does not stop here.  In the
Catholic Church, as is well known, two further steps took place.
Firstly, the Roman Rite was recast and greatly simplified in what is
called the \emph{Novus Ordo}; secondly that this was then translated,
in most places, into the vernacular, and this is how Christian Worship
is most commonly experienced by Catholics today.  Yet the story does
not stop there also.  At an international gathering---and until things
this year became so different, this was to be a very international
gathering indeed---what is the vernacular?  And so we had thought
immediately to use a language common or uncommon to all, not the
language of one party, but a language \emph{for} all.  And likewise
the liturgy did not \emph{stop} there either: the \emph{Novus Ordo}
has many adherants and continues; but so does the \emph{Vetus Ordo}.
Not a competition, not a culture war, not a political banner or
rallying point: the Mass is none of these things.  It is the worship,
by the Church on Earth, of God Almighty; whose ways are not ours, and
whose Providence will prosper as He sees fit, and not as any of us
plan (who could have seen this wedding, like this, happening last
year?).  One Mass; One Church; One Faith; One Baptism; `that Christ be
All in All'.

\bigskip

\hfill John Morris

\hfill Durham, 2020
}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "liturgy"
%%% End:
